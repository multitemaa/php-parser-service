Quick Start
-----------

Download The Project

Run the project locally
```
php -S localhost:8000 -t ./
```
THEN

```
In the URL pass the `parse_url` param 

Example GET request

`http://localhost:8000?parse_url=http://example.com/index.html`
```

It gives as a result tags from the given url content with its count
