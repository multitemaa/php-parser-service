<?php

/**
 * Enable Error Reporting
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include './app/ParserController.php';
include './app/services/dom/DomParserService.php';

(new ParserController(new DomParserService()))->parseFromHtml();
