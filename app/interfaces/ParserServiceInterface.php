<?php

interface ParserServiceInterface {

    /**
     * General method for parse data
     * @return array
     */
    public function parse() :array;

    /**
     * @return void
     */
     public function setBaseContent() :void;

    /**
     * Request to get the source data for parse
     * @param string $url
     * @param array $headers
     * @return mixed
     */
    public function query(string $url, array $headers) :string;

    /**
     * Can be used both for XML and HTML documents
     * @return mixed
     */
    public function getAllTagsFromSource() :array;

    /**
     * Can throw Validation Exception if the data is invalid
     * @throws Exception
     */
    public function validateRequestData(array $requestData) :void;

}
