<?php

include './app/services/AbstractParserService.php';

class DomParserService extends AbstractParserService
{

    /**
     * @throws Exception
     */
    public function parse() :array
    {
        $this->validateRequestData($_GET);

        $this->setHref($_GET['parse_url']);

        $this->setBaseContent();

        return array_count_values($this->getAllTagsFromSource());
    }

    /**
     * @return void
     */
    public function setBaseContent() :void
    {
        if (!$this->__SOURCE) {
            if (!$this->__SOURCE) {
                if (preg_match('/http(s)*:\/\//i', $this->__URL)) {
                    $this->setHeaders($this->getBaseCurlHeaders());
                    $this->__SOURCE = $this->query($this->__URL, $this->__HEADERS);
                } else {
                    $this->__SOURCE = file_get_contents($this->__URL);
                }
            }
        }
    }

    /**
     * @param string $url
     * @param array $headers
     * @return string
     */
    public function query(string $url, array $headers): string
    {
        $curlSession = curl_init();

        curl_setopt($curlSession, CURLOPT_URL, $url);
        for ($i = 0; $i < count($headers); $i++) {
            curl_setopt($curlSession, $headers[$i][0], $headers[$i][1]);
        }
        $returningData = curl_exec($curlSession);
        curl_close($curlSession);

        return $returningData;
    }

    /**
     * Standart headers for curl.
     * @return array[]
     */
    protected function getBaseCurlHeaders() :array
    {
        return [
            [CURLOPT_HEADER, 0],
            [CURLOPT_RETURNTRANSFER, 1],
            [CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13"],
            [CURLOPT_FOLLOWLOCATION, true],
            [CURLOPT_SSL_VERIFYHOST, false],
            [CURLOPT_SSL_VERIFYPEER, false],
            [CURLOPT_HTTPHEADER, ["Content-Type:multipart/form-data"]]
        ];
    }

    /**
     * @throws Exception
     */
    public function validateRequestData(array $requestData) :void {
        if (!isset($requestData['parse_url'])) {
            throw new Exception('parse_url не Указано', 422);
        }
    }

    /**
     * @return array
     */
    public function getAllTagsFromSource() :array {
        preg_match_all('/<([^\/!][a-z1-9]*)/i',$this->__SOURCE,$matches);
        return $matches[1] ?? [];
    }
}
