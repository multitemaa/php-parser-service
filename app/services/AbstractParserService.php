<?php
include './app/interfaces/ParserServiceInterface.php';

abstract class AbstractParserService implements ParserServiceInterface {

    /**
     * Url to file.
     * @var string
     */
    protected string $__URL = "";

    /**
     * Base Content
     * @var string
     */
    protected string $__SOURCE = "";

    /**
     * Standart headers for curl.
     * @var string
     */
    protected $__HEADERS = [];


    /**
     * Set url for parse
     * @param string $url
     */
    protected function setHref(string $url)
    {
        $this->__URL = $url;
    }

    /**
     * Set basic headers for curl
     * @param array $headers
     */
    protected function setHeaders(array $headers): void
    {
        $this->__HEADERS = $headers;
    }
}
