<?php

class ParserController {

    /**
     * @var DomParserService
     */
    private DomParserService $domParserService;

    /**
     * ParserController constructor.
     * @param DomParserService $domParserService
     */
    public function __construct(DomParserService $domParserService)
    {
        $this->domParserService = $domParserService;
    }

    /**
     * Parse from Dom Elements
     * @return void
     */
    public function parseFromHtml (): void
    {
        try {
            echo json_encode($this->domParserService->parse());
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
